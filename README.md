# Adventure
*Adventure* is a 'choose your own adventure' game.

## Game description
The player is presented with several events chosen randomly,
each representing a day. These events play out differently
depending on the choices the player makes.

## Implementation overview
The game is written in Scala.

The events are specified in XML files, allowing for easy
addition of more events in the future.

## Inspiration
The game is in part inspired by the old [Choose Your Own Adventure books](https://en.wikipedia.org/wiki/Choose_Your_Own_Adventure),
but also by modern computer games such as [Sunless Sea](http://www.failbettergames.com/sunless/) and [The Yawhg](http://www.theyawhg.com/).

## Installation
1. Download the repository from [bitbucket](https://bitbucket.org/mattiasleifsson/adventure/downloads/).
2. Extract the zip file.
3. Run the script gradlew (or gradlew.bat on windows) in the repository like this: `./gradlew distZip`
4. Extract the new zip file created in repository\_name/build/distributions
5. Run the script in the directory 'bin/' in the newly extracted directory.
6. Enjoy!
