package view

import scala.xml.XML
import model.event.Event
import model.event.GameOverEvent
import model.player.Player
import model.game.Game
import controller.Input

object View {
  val game = new Game
  def main(args: Array[String]): Unit = {
    start
  }

  def start: Unit = {
    clearConsole
    println(game.print)
    while (true) {
      Input.readInt match {
        case Some(int) => 
          clearConsole
          val event = game.continue(int)
          if (event.isInstanceOf[GameOverEvent])
            return
          println(game.print)
        case None => 
          clearConsole
          println(game.print)
      }
    }
  }

  def clearConsole: Unit = for (i <- Range(0, 10)) println("\n")
}
