package controller

import scala.util.Try

object Input {
  def readInt: Option[Int] = Try(scala.io.StdIn.readInt()).toOption
}
