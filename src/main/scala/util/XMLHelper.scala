package util

import scala.xml.XML
import scala.xml.NodeSeq
import scala.xml.Node

object XMLHelper {
  def getTagText(superTag: Node, tag: String): String = (superTag \ tag).headOption match {
    case Some(subTag) => subTag.text
    case None => throw new InvalidXMLEventException(superTag.label + " is missing " + tag)
  }
}
