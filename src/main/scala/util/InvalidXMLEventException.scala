package util

class InvalidXMLEventException(message: String) extends Exception(message)
