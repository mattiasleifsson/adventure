package model.game

import scala.xml.XML
import model.event.Event
import model.event.EmptyEvent
import model.player.Player
import java.io.File
import java.io.InputStreamReader
import java.io.BufferedReader

class Game {
  var player: Player = initPlayer
  var current: Event = loadStartEvent
  val randomEvents = loadRandomEvents
  def continue(input: Int): Event = {
    val nextEvent = current.choose(player, input)
    current = nextEvent.getOrElse(current)
    if (current.isInstanceOf[EmptyEvent]) {
      if (player.getStat("day") == 15) {
        current = loadFinalEvent
      } else {
        current = randomEvent
      }
      current.triggerEffects(player)
    }
    current
  }

  def initPlayer: Player = {
    val player = new Player
    player.setStat("strength", 10)
    player.setStat("agility", 10)
    player.setStat("intelligence", 10)
    player.setStat("money", 20)
    player.setStat("day", 0)
    player
  }

  def randomEvent = randomEvents((math.random * randomEvents.size).toInt)

  def loadStartEvent = Event("story_events/start_event.xml")

  def loadFinalEvent = Event("story_events/final_event.xml")

  def loadRandomEvents = {
    getFileNames("list_random_events.txt").map(s =>
      Event("random_events/" + s)).toVector
  }

  def getFileNames(listPath: String) = {
    val inputStream = getClass.getClassLoader.getResourceAsStream(listPath)
    val fileNames = scala.io.Source.fromInputStream(inputStream).mkString.split("\n").map(s => s).toVector
    inputStream.close()
    fileNames
  }

  def print: String = player.print + "\n" + current.print(player)
}
