package model.event

import scala.xml.XML
import scala.xml.NodeSeq
import scala.xml.Node
import scala.util.Try
import model.player.Player
import util.InvalidXMLEventException
import util.XMLHelper

object Effect {
  def apply(tag: Node): Effect = {

    def getIntValue(tag: Node): Int = {
      val value = XMLHelper.getTagText(tag, "value")
      Try(value.toInt).toOption match {
        case Some(intValue) => intValue
        case None => throw new InvalidXMLEventException("Effect has invalid value " + "'" + value + "'")
      }
    }

    val effectType = XMLHelper.getTagText(tag, "type");

    effectType match {
      case "stat" => new StatEffect(XMLHelper.getTagText(tag, "stat"), getIntValue(tag))
      case "name" => new NameEffect(XMLHelper.getTagText(tag, "value"))
      case "occurence" => new OccurenceEffect(XMLHelper.getTagText(tag, "value"))
      case "lose_occurence" => new LoseOccurenceEffect(XMLHelper.getTagText(tag, "value"))
      case unknown => throw new InvalidXMLEventException("Unknown effect type " + "'" + unknown + "'")
    }
  }
}

trait Effect {
  def trigger(player: Player): Unit
  def description: String
}

class StatEffect(val stat: String, val change: Int) extends Effect {
  def trigger(player: Player): Unit = player.incStat(stat, change)
  def description = (if (change > 0) "Increase " else "Decrease ") + stat + " by " + math.abs(change).toString
}

class NameEffect(name: String) extends Effect {
  def trigger(player: Player) = player.setName(name)
  def description = s"Your name is now $name!"
}

class OccurenceEffect(occurence: String) extends Effect {
  def trigger(player: Player) = player.addOccurence(occurence)
  def description = s"You now have occurence '$occurence'"
}

class LoseOccurenceEffect(occurence: String) extends Effect {
  def trigger(player: Player) = player.removeOccurence(occurence)
  def description = s"You no longer have occurence '$occurence'"
}
