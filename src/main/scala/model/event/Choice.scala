package model.event

import util.XMLHelper
import scala.xml.NodeSeq
import scala.xml.Node
import model.player.Player
import util.InvalidXMLEventException

object Choice {
  def apply(tag: Node): Choice = {
    val description = XMLHelper.getTagText(tag, "description");
    val requirement = getRequirement(tag)
    val consequences = (tag \ "next").map(n => getConsquence(n)).toList
    val effects = (tag \ "effect").map(e => Effect(e)).toVector
    val hidden = getHidden(tag)
    val choice = new Choice(description, requirement, consequences, effects, hidden) 
    
    if (!isValidChoice(choice)) {
      throw new InvalidXMLEventException("Choice consequences are not exhaustive")
    }
    choice
  }

  private def getConsquence(tag: Node): (Requirement, String) = {
    val requirement = getRequirement(tag)
    val nextEventString = XMLHelper.getTagText(tag, "consequence")

    (requirement, nextEventString)
  }

  private def getRequirement(tag: Node): Requirement = (tag \ "requirement").headOption match {
      case Some(req) => Requirement(tag)
      case None => new DefaultRequirement 
  }

  private def getHidden(tag: Node): Boolean = !(tag \ "hidden").isEmpty

  private def isValidChoice(choice: Choice) : Boolean = {
    val (req, _) = choice.consequences.last
    req == new DefaultRequirement
  }
}

class Choice(val description: String, val requirement: Requirement, val consequences: List[(Requirement, String)], effects: Vector[Effect], val hidden: Boolean) {

  def choose(player: Player, events: Map[String, Event]): Option[Event] = {
    if (requirement.isMet(player)) {
      for ((req, cons) <- consequences) {
        if (req.isMet(player)) {
          if (cons == "empty") {
            return Some(new EmptyEvent)
          } else if (cons == "game_over") {
            return Some(new GameOverEvent)
          }
          return Some(events(cons).addEffects(effects))
        }
      }
    }
    None
  }

  def print: String = {
    description + requirementDescription
  }

  private def requirementDescription: String = {
    if (hidden) {
      ""
    } else {
      (if (requirement == new DefaultRequirement) "" else " (" + requirement.description + ")")
    }
  }
}
