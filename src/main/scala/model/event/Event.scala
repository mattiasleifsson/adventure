package model.event

import scala.xml.XML
import scala.xml.NodeSeq
import scala.xml.Node
import model.player.Player
import util.XMLHelper
import java.io.InputStream

object Event {
  def apply(path: String): Event = {
    val file = XML.load(getClass.getClassLoader.getResourceAsStream(path))
    val loadedEvents = (file \ "load").map(l => loadEvents(l.text)).toVector
    val events: Map[String, Event] = loadedEvents.foldLeft(Map("end" -> endEvent))((a, b) => a ++ b) ++ loadEvents(path)
    events.foreach { case (s, e) => e.setEvents(events) }
    events("event0")
  }

  private def loadEvents(path: String): Map[String, Event] = {
    val file = XML.load(getClass.getClassLoader.getResourceAsStream(path))
    val title = XMLHelper.getTagText(file, "title")
    val events: Map[String, Event] = (file \ "event").map(e => (e \@ "name", getEvent(e, title))).toMap
    events
  }

  private def getEvent(tag: Node, title: String): Event = {
    new Event(title, getDescription(tag), getEffects(tag), getChoices(tag), Map())
  }

  private def getDescription(tag: Node): String = {
    XMLHelper.getTagText(tag, "description")
  }

  private def getEffects(tag: NodeSeq): Vector[Effect] = {
    (tag \ "effect").map(e => Effect(e)).toVector
  }

  private def getChoices(tag: NodeSeq): Vector[Choice] = {
    (tag \ "choice").map(e => Choice(e)).toVector
  }

  def endEvent: Event = {
    val title = "The day is over"
    val description = ""
    val effects = Vector(new StatEffect("day", 1))
    val choices = Vector(new Choice("Next day", new DefaultRequirement, List((new DefaultRequirement, "empty")), Vector(), false))
    val events = Map() : Map[String, Event]
    new Event(title, description, effects, choices, events)
  }
}

class EmptyEvent extends Event("", "", Vector(), Vector(), Map())
class GameOverEvent extends Event("", "", Vector(), Vector(), Map())

class Event (val title: String,
                     val description: String,
                     val effects: Vector[Effect],
                     val choices: Vector[Choice],
                     private var _events: Map[String, Event]) {
  def choose(player: Player, index: Int): Option[Event] = {
    if (index <= validChoices(player).size && index > 0) {
      val nextEvent = validChoices(player)(index - 1).choose(player, _events) 
      nextEvent match {
        case Some(event) => 
          event.triggerEffects(player)
          return nextEvent
        case None => return nextEvent
      }
    }
    None
  }

  def triggerEffects(player: Player): Unit = {
    effects.foreach(e => e.trigger(player))
  }

  def addEffects(newEffects: Vector[Effect]): Event = {
    new Event(title, description, effects ++ newEffects, choices, _events)
  }

  private def validChoices(player: Player): Vector[Choice] = choices.filterNot(c => c.hidden && !c.requirement.isMet(player))

  def print(player: Player): String = {
    val builder = StringBuilder.newBuilder
    builder.append(s"##$title##")
    builder.append("\n")
    builder.append(description)
    for (e <- effects) {
      builder.append("\n")
      builder.append("\n")
      builder.append(e.description)
    }
    builder.append("\n")
    builder.append("\n")
    for ((choice, i) <- validChoices(player).zipWithIndex) {
      builder.append("------------------")
      builder.append("\n")
      builder.append((i + 1).toString + ". " + choice.print)
      builder.append("\n")
    }
    builder.append("------------------")
    builder.toString()
  }

  private def setEvents(events: Map[String, Event]) = _events = events
}
