package model.event

import util.XMLHelper
import scala.xml.NodeSeq
import scala.xml.Node
import scala.util.Try
import util.InvalidXMLEventException
import util.XMLHelper
import model.player.Player

object Requirement {
  def apply(superTag: Node): Requirement = {
    new MultiRequirement((superTag \ "requirement").map(t => createRequirement(t)).toVector)
  }

  private def createRequirement(tag: Node): Requirement = {
    def getIntValue(tag: Node): Int = {
      val value = XMLHelper.getTagText(tag, "value")
      Try(value.toInt).toOption match {
        case Some(intValue) => intValue
        case None => throw new InvalidXMLEventException("Requirement has invalid value " + "'" + value + "'")
      }
    }

    def getOccurenceCount(tag: Node): Int = {
      Try(XMLHelper.getTagText(tag, "count")).toOption match {
        case Some(count) => Try(count.toInt).toOption match {
          case Some(intCount) => intCount
          case None => throw new InvalidXMLEventException("Occurence requirement has invalid count " + "'" + count + "'")
        }
        case None => 1
      }
    }

    val requirementType = XMLHelper.getTagText(tag, "type");

    requirementType match {
      case "stat" => new StatRequirement(XMLHelper.getTagText(tag, "stat"), getIntValue(tag))
      case "stat_less" => new StatLessThanRequirement(XMLHelper.getTagText(tag, "stat"), getIntValue(tag))
      case "stat_chance" => new StatChanceRequirement(XMLHelper.getTagText(tag, "stat"), getIntValue(tag))
      case "occurence" => new OccurenceRequirement(XMLHelper.getTagText(tag, "value"), getOccurenceCount(tag))
      case "no_occurence" => new NoOccurenceRequirement(XMLHelper.getTagText(tag, "value"))
      case "chance" => new ChanceRequirement(getIntValue(tag))
      case unknown => throw new InvalidXMLEventException("Unknown requirement type " + "'" + unknown + "'")
    }
  }
}

trait Requirement {
  def isMet(player: Player): Boolean
  def description: String
}

case class DefaultRequirement() extends Requirement {
  def isMet(player: Player) = true
  def description = ""
}

class StatRequirement(val stat: String, val minimumValue: Int) extends Requirement {
  def isMet(player: Player) = player.getStat(stat) >= minimumValue
  def description = "requires " + minimumValue.toString + " " + stat
}

class StatLessThanRequirement(val stat: String, val maximumValue: Int) extends Requirement {
  def isMet(player: Player) = player.getStat(stat) < maximumValue
  def description = "requires less than " + maximumValue.toString + " " + stat
}

class OccurenceRequirement(occurence: String, count: Int) extends Requirement {
  def isMet(player: Player): Boolean = player.hasOccurence(occurence, count)
  def description: String = "Requires the occurence '" + occurence + "' to have happened " + count.toString + " time(s)"
}

class NoOccurenceRequirement(occurence: String) extends Requirement {
  def isMet(player: Player): Boolean = player.hasNoOccurence(occurence)
  def description: String = "Requires the occurence '" + occurence + "' to not have happened"
}

class ChanceRequirement(chance: Int) extends Requirement {
  def isMet(player: Player): Boolean = (math.random * 100).toInt < chance
  def description: String = "Random chance to complete"
}

class StatChanceRequirement(val stat: String, val guaranteedSuccess: Int) extends Requirement {
  def isMet(player: Player) = math.random <= (player.getStat(stat).toDouble / guaranteedSuccess)
  def description = "requires " + guaranteedSuccess.toString + " " + stat + " to guarantee success"
}

class MultiRequirement(val requirements: Vector[Requirement]) extends Requirement {
  def isMet(player: Player) = requirements.foldLeft(true)((a, r) => a && r.isMet(player))
  def description = requirements.foldLeft("")((a, r) => a + ", and " + r.description).replaceFirst(", and ", "")
}
