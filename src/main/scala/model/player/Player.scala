package model.player

class Player {
  private var _name = "Unnamed hero"
  private var _occurences = Vector(): Vector[String]
  private val stats = scala.collection.mutable.Map.empty[String, Int]
  def print: String = s"$name: " + printStats
  def printStats: String = {
    stats.keys.toVector.sorted.map(k => 
      s"[$k ${stats(k)}]")
        .foldLeft("")((a, b) => a ++ " " ++ b)
  }
  def setName(name: String): Unit = _name = name
  def setStat(stat: String, value: Int): Unit = stats(stat) = value
  def incStat(stat: String, value: Int): Unit = {
    if (!stats.contains(stat))
      stats(stat) = 0
    stats(stat) += value
  }
  def getStat(stat: String): Int = stats(stat)

  def name = _name

  def addOccurence(occurence: String): Unit = _occurences = _occurences :+ occurence
  def removeOccurence(occurence: String): Unit = _occurences = _occurences.diff(Seq(occurence))
  def hasOccurence(occurence: String, count: Int): Boolean = {
    _occurences.diff(Seq.fill(count - 1)(occurence)).contains(occurence)
  }
  def hasNoOccurence(occurence: String): Boolean = !_occurences.contains(occurence)
}
